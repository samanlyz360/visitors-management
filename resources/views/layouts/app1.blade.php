<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Anlyz360') }}</title>
<!---<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">--->
    <!-- Scripts -->
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link href="{{ asset('css/style1.css') }}" rel="stylesheet">
 <style>
 .card-body1 {
    -webkit-box-flex: 1;
    flex: 1 1 auto;
    min-height: 1px;
    padding: 18px 72px 45px 59px;
    }
    .card1
    {
        width: 86%;
        margin-left: auto;
        margin-right: auto;

    }
    .logo
    {
    height: 150px;
    width: 150px;
    text-align: center;
    align-items: center;
    margin-left: auto;
    margin-right:auto;
    margin-top: 2px;
    padding-top: 0px;
   }
    @media only screen and (min-width: 412px) 
    { .header
      {
       width:108.6%;
      }
      #wrapper
      {
        width: 92%;
      }
      .meet
      {
      margin-top: 16px;
      }
      
      .logo
      {
          margin-left: auto;
          margin-right: auto;
       }
      
    @media only screen and (min-width: 446px) 
    {
      .header
      {
        width: 108%;
      }
      .meet
      {
      margin-top: 16px;
      }
     
    }
          @media only screen and (min-width: 460px) 
    {
      .header
      {
        width: 108%;
      }
       .card1
      {
         margin-left: auto;
        margin-right: auto;
          width: 386px;
      }
      .logo
      {
          margin-left: 113px;
       }
    }
     @media only screen and (min-width: 360px) 
    {
      .header
      {
        width: 108%;
      }
      .meet
      {
      margin-top: 163px;
      }
      .logo
      {
          margin-left: auto;
          margin-right: auto;
       }
    }
    </style>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light  shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}" style="font-weight:bold;color:white;">
Anlyz360
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}" style="color:white;font-weight:bold";>{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                               <!--- <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>--->
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <!--<span class="caret"></span>--->
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown" style="color:transparent;">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
</html>
