<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vistor extends Model
{
    protected $fillable = [
       
        
             'come_from','v_name','proof_name','proof_detail','mobile','meet_comp','emp_name','imagepath','pass_id','dept_name','fix_app'
    ];
}
