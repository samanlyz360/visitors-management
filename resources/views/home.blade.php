@extends('layouts.app1')
@section('content')
<html lang="en">
   <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta http-equiv="X-UA-Compatible" content="ie=edge">
      <title>Document</title>
      <script type="text/javascript" src="js/webcam.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/webcamjs/1.0.25/webcam.min.js"></script>
      <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
      <!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />--->
      <script href="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
      <link rel="stylesheet"   href="css/style.css">
      <meta name="csrf-token" content="{{ csrf_token() }}">
      <script>
         $(document).ready(function(){
             $("#show").click(function(){
             $("#my_camera").show();
           });
         });
      </script>
      <style type="text/css">
         #results
         {
         margin-top:-184px;
         margin-bottom:66px;
         height:150px;
         width:150px;
         position:absolute;
         text-align:center;
         }.navbar-light .navbar-nav .nav-link {
    color: white;
    font-size: 20px;
    font-weight: bolder;
     }
    .navbar-brand {
    float: left;
    height: 50px;
    padding: 15px 15px;
    font-size: 18px;
    line-height: 20px;
    margin-right: auto;
     }
     .btn-lg {
    padding: 4px 16px;
    font-size: 18px;
    height: 51px;
    line-height: 1.3333333;
    border-radius: 6px;
    }
         #wrapper
         {
         margin: 40px auto;
         }
         fieldset {
         min-width: 0;
         padding: 26px 26px;
         margin: 0;
         text-align: left;
         border: 0;
         }
         .selecting {
         margin: 0px 0px 0px 0px;
         padding: 6px 83px 9px 10px;
         border-radius: 11px;
         color: gray;
         }
         .selecting1 {
         margin: 0px 0px 0px 0px;
         padding: 6px 60px 9px 10px;
         border-radius: 11px;
         color: gray;
         width: 100%;
         }
         .yes
         {
         margin-top: 13px;
         position: absolute;
         padding-left: 8px;
         }
         .fix_app
         {
         margin-left: 41px;
         }
         .proff_details
         {
         margin-top:40px !important;
         }
         .get_capture
         {
         font-weight: normal;
    margin-top: 42px;
    letter-spacing: 1px;
    /* border: solid 1px gray; */
    border: 2px solid #ccc;
    border-radius: 26px;
    color: black;
    padding: 4px 9px;
         }
         #snap
         {
    border: 2px solid #ccc;
    border-radius: 26px;
    padding: 4px 8px;
    margin-top: -5px;
}
.canva_img12
{
    margin-top: 3px;
    margin-left: 26px;
}
        .video_size {
    width: 132px;
    margin-top: 78px;
    height: 123px;
    position: absolute;
    margin-left: 16px;
}
         }
          .video_size1
         {
             width: 139px;
             margin-top: 7px;
             position:absolute;
             height: 118px;
             margin-left: 16px;
         }
         .controller1
         {
          position: absolute;
          margin-top: -26px;
          margin-left: 150px;
          }
          input {
   
    border: none;
    height: 35px;
    border-radius: 3px;
    max-width: 100%;
          }
          .controls
          {
            position: absolute;
            margin-top: -346px;
            margin-left: 653px;

          }
 
         @media (min-width: 768px)
         {
.navbar-nav {
    float: right;
    margin: 0;
}
}
/*------------------------------------------------------------------
[ Focus Input ]*

.focus-input100 {
  position: absolute;
  display: block;
 /* width: calc(100% + 2px);
  height: calc(100% + 2px);*
  width:89.3%;
  height:34px;
  top: 39px;
  left: 12px;
  padding: 6px 12px;
  pointer-events: none;
  border: 2px solid #57b846;
  border-radius: 60px;

  visibility: hidden;
  opacity: 0;

  -webkit-transition: all 0.4s;
  -o-transition: all 0.4s;
  -moz-transition: all 0.4s;
  transition: all 0.4s;

  -webkit-transform: scaleX(1.1) scaleY(1.2);
  -moz-transform: scaleX(1.1) scaleY(1.2);
  -ms-transform: scaleX(1.1) scaleY(1.2);
  -o-transform: scaleX(1.1) scaleY(1.2);
  transform: scaleX(1.1) scaleY(1.2);
}

.input100:focus + .focus-input100 {
  visibility: visible;
  opacity: 1;

  -webkit-transform: scale(1);
  -moz-transform: scale(1);
  -ms-transform: scale(1);
  -o-transform: scale(1);
  transform: scale(1);
}

/*------------------------------------------------------------------*/
#switchCameraButton {
  display: none;
  width: 64px;
  height: 64px;
  background-image: url('img/ic_camera_rear_white_36px.svg');
  border-radius: 50%;
  background-color: rgba(0, 0, 0, 0.5);
}

#switchCameraButton[aria-pressed='true'] {
  background-image: url('img/ic_camera_front_white_36px.svg');
}

@media screen and (orientation: portrait) {
  /* portrait-specific styles */

  /* video_container (video) doesn't respect height... 
       so we will fill it in completely in portrait mode
    */
  #vid_container {
    width: 100%;
    height: 80%;
  }

  #gui_controls {
    width: 100%;
    height: 20%;
    left: 0;
  }

  #switchCameraButton {
    left: calc(20% - 32px);
    top: calc(50% - 32px);
  }

  #toggleFullScreenButton {
    left: calc(80% - 32px);
    top: calc(50% - 32px);
  }
}

@media screen and (orientation: landscape) {
  #vid_container {
    width: 80%;
    height: 100%;
  }

  #vid_container.left {
    left: 20%;
  }

  /* we default to right */
  #gui_controls {
    width: 20%;
    height: 100%;
    right: 0;
  }

  /* for the lefties */
  #gui_controls.left {
    left: 0;
  }

  #switchCameraButton {
    left: calc(50% - 32px);
    top: calc(18% - 32px);
  }

  #toggleFullScreenButton {
    left: calc(50% - 32px);
    top: calc(82% - 32px);
  }
}
@media only screen and (min-device-width: 768px) and (max-device-width: 1024px) and (orientation: landscape) {
    /* insert styles here */
}
 
/* (2048x1536) iPad 3 */
@media only screen and (min-device-width: 1536px) and (max-device-width: 2048px) {
    /* insert styles here */
}
 
/* (1280x720) Galaxy Note 2, WXGA */
@media only screen and (min-device-width: 720px) and (max-device-width: 1280px) {
    /* insert styles here */
}
 
/* (1366x768) WXGA Display */

 
/* (1280x1024) SXGA Display */
@media  screen and (max-width: 1280px) {
    .controls {
    position: absolute;
    margin-top: -346px;
    margin-left: 701px;
}
   
}
 
/* (1440x900) WXGA+ Display */
@media  screen and (max-width: 1440px) {
    /* insert styles here */
    .controls {
    position: absolute;
    margin-top: -352px;
    margin-left:  689px;;
}
}
@media  screen and (max-width: 1600px) and (min-width:1367px)
{
    .controls {
    position: absolute;
    margin-top: -346px;
    margin-left: 817px;
}
}

 

/* (1600x900) HD+ Display */


         @media only screen and (max-width: 460px) 
         {
         .header
         {
         width: 108%;
         }
          #wrapper
         {
         width: 92%;
         }
         .selecting
         {
         margin-top:0px;
         width: 100%;
         }
         .proff_details {
         margin-top: 5px;
         }
         .controls 
         {
         position: absolute;
         margin-top: -375px;
        margin-left: 60px;
        /* margin-top: 11px; */
        }
   
    .video_size {
    width: 202px;
    height: 114px;
    position: absolute;
    margin-left: 132px;
    margin-top: -122px;
    }
    .video_size1
    {
        margin-top: 84px;
    }
    .canva_img12 {
    margin-top: 3px;
    margin-left: -6px;
     }
         .canva_img
         {
          width: 139px;
          height: 118px;
          margin-top: 74px;
          margin-left: 145px;
          position: absolute;
         }
         }
         @media only screen and (max-width: 446px) 
         {
         .header
         {
         width: 108%;
         }
          #wrapper
         {
         width: 92%;
         }
         .meet
         {
         margin-top: 2px;
         }
         .selecting
         {
         margin-top:0px;
         width: 100%;
         }
         }
         @media only screen and (max-width: 412px) 
         { .header
         {
         width:108.6%;
         }
         #wrapper
         {
         width: 92%;
         }
         .meet
         {
         margin-top: 2px;
         }
         .selecting
         {
         margin-top:0px;
         width: 100%;
         }
         .proff_details 
         {
         margin-top: 18px !important;
         }
            .controls {
         position: absolute;
         margin-top: -372px;
         margin-left: 52px;
         /* margin-top: 11px; */
         }
         .canva_img
         {
          width: 139px;
          height: 118px;
          margin-top: 74px;
          margin-left: 145px;
          position: absolute;
         }
         }
         @media only screen and (max-width: 393px) 
         {  .selecting
         {
         padding: 6px 130px 9px 10px;
         }
         }
         @media only screen and (max-width: 360px) 
         {
         .header
         {
         width: 110.9%;
         }
         .meet
         {
         margin-top: 16px;
         }
         .proff_details {
         margin-top: 5px;
         }
         }
         * {
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

/* .buttons {
    margin: 10%;
    text-align: center;
}

.btn-hover {
    width: 200px;
    font-size: 16px;
    font-weight: 600;
    color: #fff;
    cursor: pointer;
    margin: 20px;
    height: 55px;
    text-align:center;
    border: none;
    background-size: 300% 100%;

    border-radius: 50px;
    moz-transition: all .4s ease-in-out;
    -o-transition: all .4s ease-in-out;
    -webkit-transition: all .4s ease-in-out;
    transition: all .4s ease-in-out;
}

.btn-hover:hover {
    background-position: 100% 0;
    moz-transition: all .4s ease-in-out;
    -o-transition: all .4s ease-in-out;
    -webkit-transition: all .4s ease-in-out;
    transition: all .4s ease-in-out;
}

.btn-hover:focus {
    outline: none;
}
.btn-hover.color {
    background-image: linear-gradient(to right, #009245, #FCEE21, #00A8C5, #D9E021);
    box-shadow: 0 4px 15px 0 rgba(83, 176, 57, 0.75);
    width:130px;
    height:34px;
    text-align:center;
} */

</style>
 </head>
   <body>
      <div id="wrapper" class="container" data-aos="fade-up">
         <div class="header">VISITOR'S FORM</div>
         <figure class="page-head-image">
             <br>
            <img src="images/PNG.png" alt="gate1" />
         </figure>
             <form id="form-work" class="" name="form-work" action="visitoraction" method="post">
            @csrf
            <fieldset>
               <div class="row">
                  <div class="col-sm-6">
                     <label class="control-label" for="name">Coming From:</label>
                     <input name="come_from" class="form-control" placeholder="Enter company name" type="text" value="{{ old('come_from') }}" required>
                  </div>
                  <div class="col-md-6">
                     <label class="control-label" for="name"> Visitor Name:</label>
                     <input name="v_name" class="form-control" placeholder="Enter Name"  type="text" value="{{ old('v_name') }}" required>
                  </div>
               </div>
               <div class="row">
                  <div class="col-sm-6">
                     <label>visitor id proof</label>
                     <select name="proof_name"class="selecting">
                        <option value="PAN_CARD">PAN_CARD</option>
                        <option value="AADHAAR_CARD">AADHAAR_CARD</option>
                        <option value="DRIVING_LICESCE">DRIVING_LICESCE</option>
                     </select>
                  </div>
                  <div class="col-md-6">
                     <input name="proof_detail" class="form-control proff_details" placeholder="Enter ID number" type="text" value="{{ old('proof_detail') }}" required>   
                  </div>
               </div>
               <div class="row">
                  <div class="col-md-6"><label>Mobile_number</label>
                     <input name="mobile" class="form-control" placeholder="mobile_num" type="text" value="{{ old('mobile') }}" required maxlength="10">
                                         @if ($errors->has('mobile'))
                         <span style="color:red">
                            {{ $errors->first('mobile') }}
                        </span>
                    @endif
                  </div>
                  <div class="col-sm-6">
                       @if ($errors->has('fix_app'))
                         <span style="color:red">
                            {{ $errors->first('fix_app') }}
                        </span>
                    @endif
                     <label class="control-label" for="mobile">Do you have appointment?</label></br>
                     <input type="radio" name="fix_app" value="yes"><span class="yes">Yes</span>
                     <input type="radio" name="fix_app" value="no" style="margin-left:41px;"><span class="yes">No</span>
                
                  </div>
               </div>
               <div class="row">
                  <h2 class="meet">whom to meet</h2>
               </div>
               <div class="row">
                  <div class="col-sm-6">
                     <label class="control-label" for="mobile">Company Name:</label>
                  </div>
                  <div class="col-md-6">
                     <select name="meet_comp" class="selecting1">
                        <option value="Inspirisys solution ltd   " class="selecting1">Inspirisys solution ltd </option>
                        <option value="exela technologies" class="selecting1">Exela technologies</option>
                        <option value="Odyssey Technologies" class="selecting1">Odyssey Technologies</option>
                        <option value="Others" class="selecting1">Others</option>

                     </select>
                  </div>
               </div>
               <div class="row">
                  <div class="col-sm-6">
                     <label class="control-label" for="mobile">Name:</label>
                     <input name="emp_name" class="form-control" placeholder="Emp name" type="text" value="{{ old('emp_name') }}" required>
                  </div>
                  <div class="col-md-6">
                     <label class="control-label" for="mobile"> Department:</label>
                     <input name="dept_name" class="form-control" placeholder="Enter dept" type="text" value="{{ old('dept_name') }}" required>
                  </div>
               </div>
               <div class="row">
                  <div class="col-sm-6">
                  <div class="row">
                  <div class="col-sm-12">
                     <label class="control-label" for="mobile">Visitor Pass ID No:</label>
                     <input name="pass_id" class="form-control" placeholder="Enter the proof_id" value="{{ old('pass_id') }}" type="text" required>
                  </div>
                  </div>
                  <div class="row">
                 <div class="col-sm-12">
                 <!-- Webcam video snapshot -->
                 <div class="video_size1">
<canvas id="canvas" width="640" height="480" class="canva_img12" style="width: 139px;height: 118px;"></canvas>
</div>
</div>
</div>
</div>
               <div class="col-sm-6">
              
    <video id="video" autoplay playsinline class="video_size"></video>
    </div>
    </div>
    <!-- Trigger canvas web API -->



  
               <!-- end -->
               <div class="row">
                  <div class="col-sm-12">
                      @if ($errors->has('image'))
                        <span style="color:red">
                            {{ $errors->first('image') }}
                        </span>
                    @endif
                      <input type="hidden" name="image" id="image-tag">
                     <input type="submit" class="btn btn-buttons btn-lg btn-block info">
                  </div>
               </div>
      </div>
      </div></div>
      </form>
                     <div class="controls">
      <button id="button" class="get_capture">Show camera</button>
      <select id="select" style="width: 24px;">
        <option></option>
      </select>
   <div class="controller controller1">
    <button id="snap">Capture</button>
</div>-
<div class="show">
<a href="/show" class="btn btn-default" style="postion:fixed; margin-top:-1506px;;margin-left: -476px; border-radius:15px 0px 15px 0px;font-style:italic">view entries</a>
</div>
</div>
</div>
      </fieldset> 
     
      </div>
      <script>
    const video = document.getElementById('video');
const button = document.getElementById('button');
const select = document.getElementById('select');
const canvas = document.getElementById('canvas');
const snap = document.getElementById("snap");
let currentStream;

function stopMediaTracks(stream) {
  stream.getTracks().forEach(track => {
    track.stop();
  });
}

function gotDevices(mediaDevices) {
  select.innerHTML = '';
  select.appendChild(document.createElement('option'));
  let count = 1;
  mediaDevices.forEach(mediaDevice => {
    if (mediaDevice.kind === 'videoinput') {
      const option = document.createElement('option');
      option.value = mediaDevice.deviceId;
      const label = mediaDevice.label || `Camera ${count++}`;
      const textNode = document.createTextNode(label);
      option.appendChild(textNode);
      select.appendChild(option);
    }
  });
}

button.addEventListener('click', event => {
  if (typeof currentStream !== 'undefined') {
    stopMediaTracks(currentStream);
  }
  const videoConstraints = {};
  if (select.value === '') {
    videoConstraints.facingMode = 'environment';
        
  } else {
    videoConstraints.deviceId = { exact: select.value };
  }
  const constraints = {
    video: videoConstraints,
    audio: false
  };
  navigator.mediaDevices
    .getUserMedia(constraints)
    .then(stream => {
      currentStream = stream;
      video.srcObject = stream;
      return navigator.mediaDevices.enumerateDevices();
    })
    .then(gotDevices)
    .catch(error => {
      console.error(error);
    });
});

var context = canvas.getContext('2d');
snap.addEventListener("click", function() {
	context.drawImage(video, 0, 0, 640, 480);
	var dataURL = canvas.toDataURL();
	  document.getElementById('image-tag').value = dataURL;
// 	alert(dataURL);
});

navigator.mediaDevices.enumerateDevices().then(gotDevices);


</script>  
   </body>
</html>
@endsection