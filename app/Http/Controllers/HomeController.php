<?php

namespace App\Http\Controllers;
use App\Vistor;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }
    public function action(Request $request)
    {   
        //   return $request->all();
        request()->validate([
            'come_from'     =>'required',
            'v_name'     =>'required',
            'proof_name'     =>'required',
            'proof_detail'     =>'required',
            'mobile'     =>'required|numeric',
            'fix_app'     =>'required',
            'meet_comp'     =>'required',
            'emp_name'     =>'required',
            'dept_name'     =>'required',
            'pass_id'     =>'required',
            'image'     =>'required',
            ]);
                //   return $request->all();

        $img = $request->image;
        $folderPath = "upload/";
      
        $image_parts = explode(";base64,", $img);
        $image_type_aux = explode("image/", $image_parts[0]);
        $image_type = $image_type_aux[1];
      
        $image_base64 = base64_decode($image_parts[1]);
        $fileName = uniqid() . '.jpg';
      
        $file = $folderPath . $fileName;
        file_put_contents($file, $image_base64);
      
    //     //print_r($fileName);


        
    //   return $request->all();
        $exam=new Vistor;
        $exam->come_from=$request->come_from;
        $exam->v_name=$request->v_name;
        $exam->proof_name=$request->proof_name;
        $exam->proof_detail=$request->proof_detail;
        $exam->mobile=$request->mobile;
        $exam->meet_comp=$request->meet_comp;
        $exam->emp_name=$request->emp_name;
        $exam->pass_id=$request->pass_id;
        $exam->dept_name=$request->dept_name;
        $exam->fix_app=$request->fix_app;
        $exam->imagepath="upload/".$fileName;
        $exam->save();
        $id=$exam->id;
        // $id=1;
        $visits= Vistor::find($id);
        // return $visit;
        // return view('approvel',compact('reg'));
        return view('finalform',compact('visits'));
        
    }
    public function demo(Request $request)
    {
    $img = $_POST['image'];
    $folderPath = "upload/";
  
    $image_parts = explode(";base64,", $img);
    $image_type_aux = explode("image/", $image_parts[0]);
    $image_type = $image_type_aux[1];
  
    $image_base64 = base64_decode($image_parts[1]);
    $fileName = uniqid() . '.jpg';
  
    $file = $folderPath . $fileName;
    file_put_contents($file, $image_base64);
  
    print_r($fileName);
  
    }
    public function sd(Request $request)
    {
        return $request->all();
    }
    public function show(Request $request)
    {
        $visitor=vistor::all();
        return view('show',compact ('visitor'));
    }
}
