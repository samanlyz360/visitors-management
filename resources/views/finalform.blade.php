<!DOCTYPE html>
<html>
   <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <title>Cap</title>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
      <meta name="csrf-token" content="{{ csrf_token() }}">
      <script>
$(document).ready(function(){
  $("#print").click(function(){
    $("button").hide();
    window.print();
    location.replace("/home")
  });
});
</script>
      <style>
th,td{
    text-align: left;
    border: 2px solid black;
    padding:14px;
    width:50%;
    }
    td
    {
     padding:14px14px;
    }
    .info
    {
    position: absolute;
    padding-left: 238px;
    font-weight: 500;
    letter-spacing: 1px;
    margin-top: -20px;
    }
   .special_td
   {
    padding-bottom: 0px;
   }
   .special_td1
   {
    padding-left: 6px;
   }
   .visitor_sign
   {
    padding-left: 138px;
    font-size: 16px;
    padding-top: 33px;
   }
   .whoom
   {
    margin: 2px 0px;
    font-weight: 600;
    font-size: 15px;
    text-decoration: underline;
   }
   .sign
   {
    font-weight: bold;
    padding-left: 4px;
    padding-top: 37px;
    padding-bottom: 9px;
    font-size: 16px;
    }
    label {
    display: inline-block;
    margin-bottom: .5rem;
    font-size: 16px;
    }
</style>
</head>
<body>
<br>
<div class="container" style="font-size: 11px;">
@foreach ($visits as $visit)

@endforeach


<table style="width: 100%">
 <thead>
 <tr>
   <th colspan="4"><h2 style="text-align:center;font-size: 25px;    padding-top: 3px;
">DOWLATH TOWERS</h2>
   <h5 style="text-align:center;">Kilpauk, Chennai-600010,Tamil Nadu</h5>
   </th>

  </tr>
 <tr>
   <th colspan="2" style="width: 42%;
    font-size: 16px;"><div class="special_td ">Coming From<p class="info"> :&nbsp;&nbsp;{{ $visits->come_from }}</p><br>
   <!---<input name="name1" class="form-control" placeholder="enter name" type="text" value="{{ $visits->come_from }}" readonly>
   <label class="control-label" for="name">--->Visitor Name <p class="info">:&nbsp;&nbsp;<?php echo $visits->v_name; ?></p><br>
   <!--<input name="name1" class="form-control"  type="text" value="<?php echo $visits->v_name; ?>" readonly>--->
    <div class="special_td
  control-label" for="mobile">Mobile <p class="info">:&nbsp;&nbsp;{{ $visits->mobile }}</p></div>
   <!--<input name="mobile" class="form-control"  type="text" value="{{ $visits->mobile }}" readonly>-->
    <div class="special_td control-label" for="name1">Visitor ID Proof <p class="info">:&nbsp;&nbsp;{{ $visits->proof_name }}</p></div>
   <div class="special_td control-label" for="name1"> ID Proof Details <p class="info">:&nbsp;&nbsp;{{ $visits->proof_detail }}</p></div>
   <!--<input type="text"class="form-control" value="{{ $visits->proof_name }} &nbsp;{{ $visits->proof_detail }}" readonly>-->
      

      <div class="special_td control-label" for="mobile">Visitor Pass_ID No  <p class="info">:&nbsp;&nbsp;{{ $visits->pass_id }}</p></div>
        <!-- <input name="id_no1" class="form-control"  type="text" value="  {{ $visits->pass_id }}" readonly>-->
     <h6 class="whoom">WHOOM TO MEET</h6>
 <div class="special_td control-label" for="mobile">Company Name<p class="info">:&nbsp;&nbsp;{{ $visits->meet_comp }}</p></div>
   <!--<input name="name2" class="form-control" placeholder="Enter name" type="text" value="{{ $visits->meet_comp }}" readonly>-->

<div class="special_td control-label" for="mobile"> Employee Name<p class="info">:&nbsp;&nbsp;{{ $visits->emp_name }}</p>
  <!-- <input name="dept1" class="form-control" placeholder="Enter dept" type="text" value="{{ $visits->emp_name }}" readonly>--->
</div>
<div class="special_td control-label" for="mobile">Dept Name<p class="info">:&nbsp;&nbsp;{{ $visits->dept_name }}</p>
   <!--<input name="dept2" class="form-control" placeholder="Enter name" style="color:red;" type="text" value="{{ $visits->dept_name }}" readonly>-->
</div>
</th>
 
   <th><strong style="font-size:16px">
       <label class="control-label" for="name">Register Pass_No:</label>&nbsp;&nbsp;&nbsp;{{ $visits->id }}
       <br>
<?php
date_default_timezone_set('Asia/Kolkata');
$currentTime = date( 'd-m-Y h:i:s A', time () );
echo $currentTime;
?>
</strong><br> 
<img src="<?php echo $visits->imagepath; ?>" height="110px">

 <div class="visitor_sign" >Visitor Sign</div>

<!--    <th>
<label class="control-label" for="mobile">Company Name:</label>
   <input name="name2" class="form-control" placeholder="Enter name" type="text" value="{{ $visits->meet_comp }}" readonly>

<div class="special_td1"><br><label class="control-label" for="mobile"> Employee Name:</label>
   <input name="dept1" class="form-control" placeholder="Enter dept" type="text" value="{{ $visits->emp_name }}" readonly>
</div>
<div class="special_td"><br><label class="control-label" for="mobile">Dept Name:</label>
   <input name="dept2" class="form-control" placeholder="Enter name" style="color:red;" type="text" value="{{ $visits->dept_name }}" readonly>
</div>
</th>-->
    </tr>
  
 </thead>
 <tbody>

  <tr>
  <col style="width: 24%" />
    <col style="width: 24%" />
    <col style="width: 24%" />
    </tr>
    <!--<tr>
    <th colspan="4" style="text-align:center;">Fixed Appointment {{ $visits->fix_app }}</h3></th>
    </tr>---
  <tr>
   
   <th class="special_td1"><label class="control-label" for="mobile"> Department</label>
   <input name="dept2" class="form-control" placeholder="Enter name" style="color:red;" type="text" value="{{ $visits->dept_name }}">
   </th>
   <th class="special_td1">Mail Id:</th>
   <th class="special_td1">ID No:</th>
  </tr>
 --->
 </tbody>
 <tfoot>
  
   <th colspan="2" class="sign">Security Signature</th>
     <td class="sign">Authority Sign</td>
  </tr>
 
  <col style="width: 24%" />
     <col style="width: 24%" />
    </tr>
 </tfoot>
</table>
<div class="row">
<div class="col-sm-10">
    <button id="print">Print this page</button>

</div>

</div>

</body>
</html>