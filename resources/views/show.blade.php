@extends('layouts.app1')
@section('content')
<html lang="en">
   <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta http-equiv="X-UA-Compatible" content="ie=edge">
      <title>Document</title>
      <script type="text/javascript" src="js/webcam.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/webcamjs/1.0.25/webcam.min.js"></script>
      <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
      <!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />--->
      <script href="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
      <link rel="stylesheet"   href="css/style.css">
      <meta name="csrf-token" content="{{ csrf_token() }}">
      <script>
         $(document).ready(function(){
             $("#show").click(function(){
             $("#my_camera").show();
           });
         });
      </script>
      <style type="text/css">
         
    .navbar-brand {
    float: left;
    height: 50px;
    padding: 15px 15px;
    font-size: 18px;
    line-height: 20px;
    margin-right: auto;
     }
     .btn-lg {
    padding: 4px 16px;
    font-size: 18px;
    height: 51px;
    line-height: 1.3333333;
    border-radius: 6px;
    } 
         #wrapper
         {
         margin: 40px auto;
         }
          fieldset {
         min-width: 0;
         padding: 26px 26px;
         margin: 0;
         text-align: left;
         border: 0;
         }
         .selecting {
         margin: 0px 0px 0px 0px;
         padding: 6px 83px 9px 10px;
         border-radius: 11px;
         color: gray;
         } 
          .selecting1 {
         margin: 0px 0px 0px 0px;
         padding: 6px 60px 9px 10px;
         border-radius: 11px;
         color: gray;
         width: 100%;
         }
         .yes
         {
         margin-top: 13px;
         position: absolute;
         padding-left: 8px;
         } 
          .fix_app
         {
         margin-left: 41px;
         }
         .proff_details
         {
         margin-top:40px !important;
         }
         .get_capture
         {
         font-weight: normal;
    margin-top: 42px;
    letter-spacing: 1px;
     border: solid 1px gray; 
     border: 2px solid #ccc;
    border-radius: 26px;
    color: black;
    padding: 4px 9px;
         }  
          #snap
         {
    border: 2px solid #ccc;
    border-radius: 26px;
    padding: 4px 8px;
    margin-top: -5px;
}
.canva_img12
{
    margin-top: 3px;
    margin-left: 26px;
}
        .video_size {
    width: 132px;
    margin-top: 78px;
    height: 123px;
    position: absolute;
    margin-left: 16px;
}
         }
          .video_size1
         {
             width: 139px;
             margin-top: 7px;
             position:absolute;
             height: 118px;
             margin-left: 16px;
         }
         .controller1
         {
          position: absolute;
          margin-top: -26px;
          margin-left: 150px;
          }
          input {
   
    border: none;
    height: 35px;
    border-radius: 3px;
    max-width: 100%;
          }
          .controls
          {
            position: absolute;
            margin-top: -346px;
            margin-left: 653px;

          }
 
         @media (min-width: 768px)
         {
.navbar-nav {
    float: right;
    margin: 0;
}
} 
/*------------------------------------------------------------------
[ Focus Input ]*

.focus-input100 {
  position: absolute;
  display: block;
 /* width: calc(100% + 2px);
  height: calc(100% + 2px);*
  width:89.3%;
  height:34px;
  top: 39px;
  left: 12px;
  padding: 6px 12px;
  pointer-events: none;
  border: 2px solid #57b846;
  border-radius: 60px;

  visibility: hidden;
  opacity: 0;

  -webkit-transition: all 0.4s;
  -o-transition: all 0.4s;
  -moz-transition: all 0.4s;
  transition: all 0.4s;

  -webkit-transform: scaleX(1.1) scaleY(1.2);
  -moz-transform: scaleX(1.1) scaleY(1.2);
  -ms-transform: scaleX(1.1) scaleY(1.2);
  -o-transform: scaleX(1.1) scaleY(1.2);
  transform: scaleX(1.1) scaleY(1.2);
}

.input100:focus + .focus-input100 {
  visibility: visible;
  opacity: 1;

  -webkit-transform: scale(1);
  -moz-transform: scale(1);
  -ms-transform: scale(1);
  -o-transform: scale(1);
  transform: scale(1);
}

/*------------------------------------------------------------------*/
 #switchCameraButton {
  display: none;
  width: 64px;
  height: 64px;
  background-image: url('img/ic_camera_rear_white_36px.svg');
  border-radius: 50%;
  background-color: rgba(0, 0, 0, 0.5);
}

 #switchCameraButton[aria-pressed='true'] {
  background-image: url('img/ic_camera_front_white_36px.svg');
} 

@media screen and (orientation: portrait) {
  portrait-specific styles */

  /* video_container (video) doesn't respect height... 
       so we will fill it in completely in portrait mode
    */
  #vid_container {
    width: 100%;
    height: 80%;
  }

  #gui_controls {
    width: 100%;
    height: 20%;
    left: 0;
  }

  #switchCameraButton {
    left: calc(20% - 32px);
    top: calc(50% - 32px);
  }

  #toggleFullScreenButton {
    left: calc(80% - 32px);
    top: calc(50% - 32px);
  }
}

@media screen and (orientation: landscape) {
  #vid_container {
    width: 80%;
    height: 100%;
  }

  #vid_container.left {
    left: 20%;
  }

  /* we default to right */
  #gui_controls {
    width: 20%;
    height: 100%;
    right: 0;
  }

  /* for the lefties */
  #gui_controls.left {
    left: 0;
  }

  #switchCameraButton {
    left: calc(50% - 32px);
    top: calc(18% - 32px);
  }

  #toggleFullScreenButton {
    left: calc(50% - 32px);
    top: calc(82% - 32px);
  }
}
@media only screen and (min-device-width: 768px) and (max-device-width: 1024px) and (orientation: landscape) {
    /* insert styles here */
}
 
/* (2048x1536) iPad 3 */
@media only screen and (min-device-width: 1536px) and (max-device-width: 2048px) {
    /* insert styles here */
}
 
/* (1280x720) Galaxy Note 2, WXGA */
@media only screen and (min-device-width: 720px) and (max-device-width: 1280px) {
    /* insert styles here */
}
 
/* (1366x768) WXGA Display */

 
/* (1280x1024) SXGA Display */
@media  screen and (max-width: 1280px) {
    .controls {
    position: absolute;
    margin-top: -346px;
    margin-left: 701px;
}
   
}
 
/* (1440x900) WXGA+ Display */
@media  screen and (max-width: 1440px) {
    /* insert styles here */
    .controls {
    position: absolute;
    margin-top: -352px;
    margin-left:  689px;;
}
}
@media  screen and (max-width: 1600px) and (min-width:1367px)
{
    .controls {
    position: absolute;
    margin-top: -346px;
    margin-left: 817px;
}
}
#wrapper
         {
         width: 92%;
         }
 

/* (1600x900) HD+ Display */


         @media only screen and (max-width: 460px) 
         {
         .header
         {
         width: 108%;
         }
          #wrapper
         {
         width: 92%;
         }
         .selecting
         {
         margin-top:0px;
         width: 100%;
         }
         .proff_details {
         margin-top: 5px;
         }
         .controls 
         {
         position: absolute;
         margin-top: -375px;
        margin-left: 60px;
        /* margin-top: 11px; */
        }
   
    .video_size {
    width: 202px;
    height: 114px;
    position: absolute;
    margin-left: 132px;
    margin-top: -122px;
    }
    .video_size1
    {
        margin-top: 84px;
    }
    .canva_img12 {
    margin-top: 3px;
    margin-left: -6px;
     }
         .canva_img
         {
          width: 139px;
          height: 118px;
          margin-top: 74px;
          margin-left: 145px;
          position: absolute;
         }
         }
         @media only screen and (max-width: 446px) 
         {
         .header
         {
         width: 108%;
         }
          #wrapper
         {
         width: 92%;
         }
         .meet
         {
         margin-top: 2px;
         }
         .selecting
         {
         margin-top:0px;
         width: 100%;
         }
         }
         @media only screen and (max-width: 412px) 
         { .header
         {
         width:108.6%;
         }
         #wrapper
         {
         width: 92%;
         }
         .meet
         {
         margin-top: 2px;
         }
         .selecting
         {
         margin-top:0px;
         width: 100%;
         }
         .proff_details 
         {
         margin-top: 18px !important;
         }
            .controls {
         position: absolute;
         margin-top: -372px;
         margin-left: 52px;
         /* margin-top: 11px; */
         }
         .canva_img
         {
          width: 139px;
          height: 118px;
          margin-top: 74px;
          margin-left: 145px;
          position: absolute;
         }
         }
         @media only screen and (max-width: 393px) 
         {  .selecting
         {
         padding: 6px 130px 9px 10px;
         }
         }
         @media only screen and (max-width: 360px) 
         {
         .header
         {
         width: 110.9%;
         }
         .meet
         {
         margin-top: 16px;
         }
         .proff_details {
         margin-top: 5px;
         }
         }
.table>tbody>tr>td{
    padding: 8px;
    line-height: 1.42857143;
    vertical-align: top;
    border-top: none;
}
* {
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

.buttons {
    margin: 10%;
    text-align: center;
}

.btn-hover {
    width: 200px;
    font-size: 16px;
    font-weight: 600;
    color: #fff;
    cursor: pointer;
    margin: 20px;
    height: 55px;
    text-align:center;
    border: none;
    background-size: 300% 100%;

    border-radius: 50px;
    moz-transition: all .4s ease-in-out;
    -o-transition: all .4s ease-in-out;
    -webkit-transition: all .4s ease-in-out;
    transition: all .4s ease-in-out;
}

.btn-hover:hover {
    background-position: 100% 0;
    moz-transition: all .4s ease-in-out;
    -o-transition: all .4s ease-in-out;
    -webkit-transition: all .4s ease-in-out;
    transition: all .4s ease-in-out;
}

.btn-hover:focus {
    outline: none;
}
.btn-hover.color-6 {
    background-image: linear-gradient(to right, #009245, #FCEE21, #00A8C5, #D9E021);
    box-shadow: 0 4px 15px 0 rgba(83, 176, 57, 0.75);
    width:130px;
    height:34px;
    text-align:center;
}

         
</style>
 </head>
   <body>
<div class="ll">
<a href="/home" class="btn-hover color-6" style="position:fixed;border-radius:15px 0px 15px 0px;font-style:italic">Visitor's Form</a>
</div>
<div class="lk container" ><br>
<table border = "1" class="table table-borderless"style="background-color:white;border-radius: 25px;border: none;">
<tr style="font-weight: bold;">
<td class="e" style="padding-left: 20px;padding-top: 16px">Id</td>
<td>Visitor Name</td>
<td>Visitor Proof</td>
<td>Visitor Proof Details</td>
<td>Mobile Number</td>
<td>Appointment</td>
<td>Company Name</td>
<td>Employee Name</td>
<td>Image Path</td>
<td>Visitor Pass Id</td>
<td>Dept Name</td>
<td>Fix App</td>
</tr>
@foreach ($visitor as $user)
<tr>
<td class="e" style="padding-left: 20px;padding-top: 16px">{{ $user->id }}</td>
<td>{{ $user->come_from }}</td>
<td>{{ $user->v_name }}</td>
<td>{{ $user->proof_name }}</td>
<td>{{ $user->proof_detail }}</td>
<td>{{ $user->mobile }}</td>
<td>{{ $user->meet_comp }}</td>
<td>{{ $user->emp_name }}</td>
<td><img src="{{ $user->imagepath }}" style="width:100px;height:100px;padding:6px"></td>
<td>{{ $user->pass_id }}</td>
<td>{{ $user->dept_name }}</td>
<td>{{ $user->fix_app }}</td>
</tr>
@endforeach
</table>

</div>

   </body>
</html>
@endsection
<!-- end -->

