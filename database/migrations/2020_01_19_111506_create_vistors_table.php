<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVistorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vistors', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('come_from');
            $table->string('v_name');
            $table->string('proof_name');
            $table->string('proof_detail');
            $table->biginteger('mobile');
            $table->string('meet_comp');
            $table->string('emp_name');
            $table->string('imagepath');
            $table->string('pass_id');
            $table->string('dept_name');
            $table->string('fix_app');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vistors');
    }
}
