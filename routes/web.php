<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::post('/visitoraction','HomeController@action');
Route::post('upload','HomeController@upload');
// Route::get('sample', function () {
//     return view('webcam');
// });
Route::get('sample', function () {
    return view('sample');
});
Route::post('storeimage','HomeController@demo');
Route::get('/print', function () {
    return view('finalform');
});
Route::get('sam', function () {
    return view('show');
});
Route::get('show','HomeController@show');